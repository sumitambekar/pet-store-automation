package starter.stepdefinitions;

import static net.serenitybdd.rest.SerenityRest.lastResponse;
import static net.serenitybdd.rest.SerenityRest.restAssuredThat;
import static org.hamcrest.CoreMatchers.equalTo;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.cucumber.datatable.DataTable;
//import io.cucumber.java.en.And;
//import io.cucumber.java.en.Given;
//import io.cucumber.java.en.Then;
//import io.cucumber.java.en.When;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import net.serenitybdd.rest.SerenityRest;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class PetApiStepDefinitions {

    public static JSONObject msgBody = new JSONObject();

    @Then("Response has name {string}") public void responseHasName(String name) {
        restAssuredThat(lastResponse -> lastResponse.body("name", equalTo(name)));
        lastResponse().prettyPrint();
    }

    @Given("I provide below information to create a new pet entry")
    public void iProvideBelowInformationToCreateANewPetEntry(DataTable petInfo)
        throws IOException, ParseException {
        List<Map<String, String>> newPetInfos = petInfo.asMaps(String.class, String.class);

        JSONParser parser = new JSONParser();
        String inputRequest = "./src/test/resources/inputFiles/createPetRequest.json";
        Object obj = parser.parse(new FileReader(inputRequest));

        msgBody.clear();
        msgBody = (JSONObject) obj;

        for (Map<String, String> field : newPetInfos) {
            msgBody.put((field.values().toArray())[0].toString(),
                ((field.values().toArray())[1]).toString());
        }
    }

    @When("I make pet api {string} call to {string}")
    public void iMakePetApiCallTo(String method, String endPoint) throws IOException {

        switch (method.toUpperCase()) {
            case "POST":
                SerenityRest.given().relaxedHTTPSValidation().contentType("application/json")
                    .body(msgBody.toString()).when().post(endPoint);
                break;
            case "PUT":
                SerenityRest.given().relaxedHTTPSValidation().contentType("application/json")
                    .body(msgBody.toString()).when().put(endPoint);
                break;
            case "DELETE":
                SerenityRest.given().relaxedHTTPSValidation().contentType("application/json")
                    .body(msgBody.toString()).when().delete(endPoint);
                break;
            case "GET":
                SerenityRest.given().relaxedHTTPSValidation().contentType("application/json")
                    .body(msgBody.toString()).when().get(endPoint);
                break;
            default:
                break;
        }
    }



    @And("I verify pet status is {string}") public void iVerifyPetStatusIs(String petStatus) {
        restAssuredThat(lastResponse -> lastResponse.body("status", equalTo(petStatus)));
    }

    @Given("I read request file {string}") public void iReadRequestFile(String fileName)
        throws IOException, ParseException {
        JSONParser parser = new JSONParser();
        String inputRequest = "./src/test/resources/inputFiles/" + fileName;
        Object obj = parser.parse(new FileReader(inputRequest));

        msgBody.clear();
        msgBody = (JSONObject) obj;
    }

}
