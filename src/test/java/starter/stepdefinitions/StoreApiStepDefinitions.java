package starter.stepdefinitions;

import static net.serenitybdd.rest.SerenityRest.restAssuredThat;
import static org.hamcrest.CoreMatchers.equalTo;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import io.cucumber.datatable.DataTable;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import net.serenitybdd.rest.SerenityRest;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

//import io.cucumber.java.en.And;
//import io.cucumber.java.en.Given;
//import io.cucumber.java.en.When;

public class StoreApiStepDefinitions {

    public static JSONObject msgBody = new JSONObject();

    @Given("I provide below information to create a purchase order")
    public void iProvideBelowInformationToCreateAPurchaseOrder(DataTable orderInfo)
        throws IOException, ParseException {

        List<Map<String, String>> newOrderInfos = orderInfo.asMaps(String.class, String.class);

        JSONParser parser = new JSONParser();
        String inputRequest = "./src/test/resources/inputFiles/createPurchaseOrder.json";
        Object obj = parser.parse(new FileReader(inputRequest));

        msgBody.clear();
        msgBody = (JSONObject) obj;

        for (Map<String, String> field : newOrderInfos) {
            msgBody.put((field.values().toArray())[0].toString(),
                ((field.values().toArray())[1]).toString());
        }
    }

    @When("I make store api {string} call to {string}")
    public void iMakeStoreApiCallTo(String method, String endPoint) {
        switch (method.toUpperCase()) {
            case "POST":
                SerenityRest.given().relaxedHTTPSValidation().contentType("application/json")
                    .body(msgBody.toString()).when().post(endPoint);
                break;
            case "DELETE":
                SerenityRest.given().relaxedHTTPSValidation().contentType("application/json")
                    .body(msgBody.toString()).when().delete(endPoint);
                break;
            case "GET":
                SerenityRest.given().relaxedHTTPSValidation().contentType("application/json")
                    .body(msgBody.toString()).when().get(endPoint);
                break;
            default:
                break;
        }
    }

    @And("I verify store response has below information")
    public void iVerifyStoreResponseHasBelowInformation(DataTable fields) {
        List<Map<String, String>> validations = fields.asMaps(String.class, String.class);
        for (Map<String, String> validation : validations) {
            restAssuredThat(lastResponse -> lastResponse
                .body((validation.values().toArray())[0].toString(),
                    equalTo(Integer.parseInt((validation.values().toArray())[1].toString()))));
        }
    }
}
