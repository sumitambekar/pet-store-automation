package starter.stepdefinitions;

import static net.serenitybdd.rest.SerenityRest.restAssuredThat;

import cucumber.api.java.en.Then;

//import io.cucumber.java.en.Then;

public class CommonStepDefinitions {
    @Then("I get status code {string}") public void iGetStatusCode(String statusCode) {
        restAssuredThat(lastResponse -> lastResponse.statusCode(Integer.parseInt(statusCode)));
    }
}
