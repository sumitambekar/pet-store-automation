@petNegativeScenarios @petStore
Feature: Negative scenarios for Pet API

  Scenario: Pet not found
    When I make pet api "GET" call to "https://petstore.swagger.io/v2/pet/5479459347293742"
    Then I get status code "404"

    # This is a bug. Status code should be 400 but getting 200.
#  Scenario: Find pet by invalid status
#    When I make pet api "GET" call to "https://petstore.swagger.io/v2/pet/findByStatus?status=something"
#    Then I get status code "400"

  Scenario: Delete a non existing pet
    When I make pet api "DELETE" call to "https://petstore.swagger.io/v2/pet/5479459347293742"
    Then I get status code "404"
