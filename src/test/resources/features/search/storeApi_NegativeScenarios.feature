@storeNegativeScenarios @petStore
Feature: Store api negative scenarios

  Scenario: Find purchase order by invalid ID
    Given I make store api "GET" call to "https://petstore.swagger.io/v2/store/order/9"
    Then I get status code "404"


  Scenario: Delete invalid purchase order by ID
    Given I make store api "DELETE" call to "https://petstore.swagger.io/v2/store/order/758498"
    Then I get status code "404"

