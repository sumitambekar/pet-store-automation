@petPositiveScenarios @petStore
Feature: Everything about your Pets

  Scenario: Add a new pet to the Store
    Given I provide below information to create a new pet entry
      | jsonPaths | values    |
      | id        | 57495740  |
      | name      | katie     |
      | status    | available |
    When I make pet api "POST" call to "https://petstore.swagger.io/v2/pet"
    Then I get status code "200"

  Scenario: Update pet information of an existing pet
    Given I provide below information to create a new pet entry
      | jsonPaths | values   |
      | id        | 57495740 |
      | name      | katie    |
      | status    | pending  |
    When I make pet api "PUT" call to "https://petstore.swagger.io/v2/pet"
    Then I get status code "200"
    And I verify pet status is "pending"

  Scenario Outline: Update pet information of an existing pet
    When I make pet api "GET" call to "https://petstore.swagger.io/v2/pet/findByStatus?status=<status>"
    Then I get status code "200"
    Examples:
      | status    |
      | available |
      | pending   |
      | sole      |

  Scenario Outline: Search pet by ID
    When I make pet api "GET" call to "https://petstore.swagger.io/v2/pet/<petId>"
    Then I get status code "200"
    Then Response has name "katie"
    Examples:
      | petId    |
      | 57495740 |

  Scenario Outline: Delete a pet entry
    When I make pet api "DELETE" call to "https://petstore.swagger.io/v2/pet/<petId>"
    Then I get status code "200"
    Examples:
      | petId    |
      | 57495740 |
