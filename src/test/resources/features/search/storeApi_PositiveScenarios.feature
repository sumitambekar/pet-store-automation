@storePositiveScenarios @petStore
Feature: Everything about stores

  Scenario Outline: Place an order for a pet
    Given I provide below information to create a purchase order
      | jsonPaths | values     |
      | id        | <id>       |
      | petId     | <petId>    |
      | quantity  | <quantity> |
    When I make store api "POST" call to "https://petstore.swagger.io/v2/store/order"
    Then I get status code "200"
    And I verify store response has below information
      | field    | value      |
      | id       | <id>       |
      | petId    | <petId>    |
      | quantity | <quantity> |

    Examples:
      | id | petId | quantity |
      | 5  | 3     | 1        |


  Scenario: Find purchase order by ID
    Given I make store api "GET" call to "https://petstore.swagger.io/v2/store/order/5"
    Then I get status code "200"
    And I verify store response has below information
      | field | value |
      | id    | 5     |

  Scenario: Delete purchase order by ID
    Given I make store api "DELETE" call to "https://petstore.swagger.io/v2/store/order/5"
    Then I get status code "200"

  Scenario: Returns pet inventories by status
    Given I make store api "GET" call to "https://petstore.swagger.io/v2/store/inventory"
    Then I get status code "200"

