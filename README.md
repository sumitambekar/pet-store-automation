# LeasePlanTest

Steps to execute the tests.
1. cucumber tag should be provided in <tag></tag> in pom.xml. (e.g. <tag>@petStore</tag>)
2. exeute the command "mvn verify -Pexceute" to run scenarios with tag mentioned in step 1.
3. execute the command "mvn serenity:aggregate" to generate single page html report. Report can be found at target/site/serenity/index.html

ToDos:
1. More scenarios can be written to make testing complete.
2. Reporting needs improvement.
3. More serenity BDD feature can be used for more clean code.